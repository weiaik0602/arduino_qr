#include <Arduino.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "qrcodegen.h"


byte pixels[100* 100];

static void printQr(const uint8_t qrcode[]) {
	int size = qrcodegen_getSize(qrcode);
  Serial.print(size);
  Serial.println(sizeof(pixels)/sizeof(pixels[0]));

	for (int y = 0; y < size; y++) {
		for (int x = 0; x < size; x++) {
      if (qrcodegen_getModule(qrcode,x,y)){
				for(int i=0;i<8;i++){
					pixels[y * size*8 + x +size*i] = (byte)0xff;
				}
			}else{
				for(int i=0;i<8;i++){
					pixels[y * size*8 + x +size*i] = (byte)0x00;
				}
			// Serial.print((qrcodegen_getModule(qrcode, x, y) ? "██" : "  "));
      // pixels[y* size + x]= (qrcodegen_getModule(qrcode, x, y) ? 0xFF : 0x00 );
		}
		//Serial.print("\n");
	}
	//Serial.print("\n");
  }
  for(int i=0;i< (size*size*8) ; i++){
    Serial.print(pixels[i]);
    Serial.print(' ');
    yield();
  }
  //Serial.println(pixels);
}


void setup() {
  Serial.begin(9600);
  const char *text = "s;{\"b\":\"laundries\",\"j\":\"1-WYIEIVI6I8YDW3RR8Q\",\"v\":\"5d6be3ab4f232723d3929777\"}";                // User-supplied text
	enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level
	
	// Make and print the QR Code symbol
	uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
	uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];
	bool ok = qrcodegen_encodeText(text, tempBuffer, qrcode, errCorLvl,
		qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
	if (ok)
		printQr(qrcode);
}

void loop() {
  // put your main code here, to run repeatedly:
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// #include <Arduino.h>
// #include "qrcode.h"

// byte pixels[100* 100];

// void setup() {
//     Serial.begin(9600);

//     // Start time
//     uint32_t dt = millis();
  
//     // Create the QR code
//     QRCode qrcode, qrcode_2;
//     uint8_t qrcodeData[qrcode_getBufferSize(3)];
//     uint8_t qrcodeData2[qrcode_getBufferSize(9)];
//     qrcode_initText(&qrcode, qrcodeData, 3, 0, "HELLO WORLD");
//     Serial.println(qrcode.size);
//     qrcode_initText(&qrcode_2, qrcodeData2, 9, 0, "HELLO WORLD");
//     Serial.println(qrcode_2.size);
//     // Delta time
//     dt = millis() - dt;
//     Serial.print("QR Code Generation Time: ");
//     Serial.print(dt);
//     Serial.print("\n");

//     // Top quiet zone
//     Serial.print("\n\n\n\n");

//     for (uint8_t y = 0; y < qrcode_2.size; y++) {

//         // Each horizontal module
//         for (uint8_t x = 0; x < qrcode_2.size; x++) {

//             // Print each module (UTF-8 \u2588 is a solid block)
//             Serial.print(qrcode_getModule(&qrcode_2, x, y) ? "██": "  ");
//             pixels[y* qrcode_2.size + x]= (qrcode_getModule(&qrcode_2, x, y) ? 0xFF : 0x00 );
//         }
//         yield();
//         Serial.print("\n");
//     }
//     // Bottom quiet zone
//     Serial.print("\n\n\n\n");

//     for(int i=0;i< (441) ; i++){
//       Serial.print(pixels[i]);
//       Serial.print(' ');
//       yield();
//     }
// }

// void loop() {

// }